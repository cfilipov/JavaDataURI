/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.vopilif.util.datauri;

import eu.medsea.mimeutil.MimeUtil2;
import eu.medsea.mimeutil.detector.MagicMimeMimeDetector;
import net.iharder.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;

/**
 * Utility for creating data URIs (RFC 2397).
 * <p/>
 * This class consists of static helper methods which facilitate the creation of
 * data URIs. A data URI may be generated from a file, URL or byte array. In the
 * case of file and URL the MIME type will be detected (when possible). When
 * encoding a byte array you must provide a MIME type.
 * <p/>
 * This code has not had a great deal of attention paid to error handling.
 * Beware.
 *
 * @see <a href="http://www.ietf.org/rfc/rfc2397.txt">RFC 2397 (IETF)</a>
 * @see DataURIEmbedder
 * @see LinkFormat
 */
public class DataURI {

    private static final MimeUtil2 mimeUtil = new MimeUtil2();

    static {
        mimeUtil.registerMimeDetector(MagicMimeMimeDetector.class.getName());
    }

    /**
     * Encode the contents retrieved from the URL into a data URI. The MIME type
     * will be resolved by the downloaded file content type or extension if
     * available (in that order).
     *
     * @param url The URL whose response will be encoded as a data URI.
     * @return The data URI as a String.
     * @throws IOException
     */
    public static String encode(URL url) throws IOException {
        return DataURI.encode(url, null);
    }

    /**
     * Encode the contents retrieved from the URL into a data URI using the MIME
     * type provided.
     *
     * @param url      The URL whose response will be encoded as a data URI.
     * @param mimeType The MIME type to use in the data URI.
     * @return The data URI as a String.
     * @throws IOException
     */
    public static String encode(URL url, String mimeType) throws IOException {
        URLConnection connection = url.openConnection();
        if (mimeType == null) {
            mimeType = connection.getContentType();
            if (mimeType == null) {
                mimeType = DataURI.getMimeType(new File(url.getFile()));
            }
        }
        InputStream in = connection.getInputStream();
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            for (int c = in.read(); c != -1; c = in.read()) {
                byteStream.write(c);
            }
            byteStream.flush();
        } finally {
            in.close();
        }
        return DataURI.encode(byteStream.toByteArray(), mimeType);
    }

    /**
     * Encode the contents of a file into a data URI. The MIME type
     * will be resolved by the file content type or extension if available (in
     * that order).
     *
     * @param file The file whose data to encode.
     * @return The data URI as a String.
     * @throws IOException
     */
    public static String encode(File file) throws IOException {
        return DataURI.encode(file, null);
    }

    /**
     * Encode the contents of a file into a data URI and use the given MIME
     * type.
     *
     * @param file     The file whose data to encode.
     * @param mimeType The MIME type to use in the data URI.
     * @return The data URI as a String.
     * @throws IOException
     */
    public static String encode(File file, String mimeType) throws IOException {
        assert file != null;
        if (mimeType == null) {
            mimeType = DataURI.getMimeType(file);
        }
        final String base64Data = Base64.encodeFromFile(file.getCanonicalPath());
        return DataURI.makeDataURI(base64Data, mimeType);
    }

    /**
     * Encode the byte array into a data URI.
     *
     * @param bytes    The data to encode into a data URI.
     * @param mimeType The MIME type to use in the data URI.
     * @return The data URI as a String.
     * @throws IOException
     */
    public static String encode(byte[] bytes, String mimeType) throws IOException {
        assert mimeType != null;
        final String base64Data = Base64.encodeBytes(bytes);
        return DataURI.makeDataURI(base64Data, mimeType);
    }

    private static String makeDataURI(String base64Data, String mimeType) {
        assert base64Data != null;
        assert mimeType != null;
        return (new StringBuilder())
                .append("data:")
                .append(mimeType)
                .append(";base64,")
                .append(base64Data).toString();
    }

    private static String getMimeType(File file) {
        assert file != null;
        final Collection possibleMimeTypes = DataURI.mimeUtil.getMimeTypes(file);
        return MimeUtil2.getMostSpecificMimeType(possibleMimeTypes).toString();
    }
}
