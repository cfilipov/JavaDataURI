/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.vopilif.util.datauri;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

import java.util.ArrayList;

/**
 * This is a quick and dirty hack to support {@link LinkFormat} [] fields by
 * implementing a custom {@link OptionHandler}. Args4j defines option handlers
 * for Enum and for List<String> but does not define a handler than can support
 * a List of enums. This class defines a handler specifically for the
 * {@link LinkFormat} enum. The
 * {@link LinkFormatOptionHandler#parseArguments(org.kohsuke.args4j.spi.Parameters)}
 * implementation is almost identical to that of
 * {@link org.kohsuke.args4j.spi.StringArrayOptionHandler}, in fact it was
 * mostly copied from that class. Similarly,
 * {@link com.vopilif.util.datauri.LinkFormatOptionHandler#getDefaultMetaVariable()}
 * and the code in {@link LinkFormatOptionHandler#getEnumConstant(String)} are
 * pulled from {@link org.kohsuke.args4j.spi.EnumOptionHandler}.
 */
public class LinkFormatOptionHandler extends OptionHandler<LinkFormat[]> {

    private final Class<LinkFormat> enumType = LinkFormat.class;

    public LinkFormatOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super LinkFormat[]> setter) {
        super(parser, option, setter);
    }

    @Override
    public int parseArguments(Parameters params) throws CmdLineException {
        int counter = 0;
        ArrayList<LinkFormat> values = new ArrayList<LinkFormat>();

        while(true) {
            String param;
            try {
                param = params.getParameter(counter);
            } catch (CmdLineException ex) {
                break;
            }
            if(param.startsWith("-")) {
                break;
            }

            for (String str : param.split(" ")) {
                values.add(this.getEnumConstant(str));
            }
            counter++;
        }

        // to work around a javac bug in Java1.5, we need to first assign this to
        // the raw type.
        Setter s = this.setter;
        s.addValue(values.toArray(new LinkFormat[values.size()]));
        return counter;
    }

    private LinkFormat getEnumConstant(String str) throws CmdLineException {
        LinkFormat value = null;
        for( LinkFormat o : enumType.getEnumConstants() )
            if(o.name().equalsIgnoreCase(str)) {
                value = o;
                break;
            }
        if(value==null)
            throw new CmdLineException(owner, "No such option: " + str);
        return value;
    }

    @Override
    public String getDefaultMetaVariable() {
        StringBuilder rv = new StringBuilder();
        rv.append("[");
        for (LinkFormat format : enumType.getEnumConstants()) {
            rv.append(format).append(" | ");
        }
        rv.delete(rv.length()-3, rv.length());
        rv.append("]");
        return rv.toString();
    }
}
