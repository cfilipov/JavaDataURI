/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.vopilif.util.datauri;

import java.util.regex.Pattern;

/**
 * Enumeration of common (at least for the author's purposes) link formats and
 * regular expression patterns used to match them. These patterns allow one to
 * match the link format within a text document as well as extract the URL
 * component.
 * <p/>
 * Each LinkFormat contains a pre-compiled regular expression pattern. The
 * pattern will contain 3 capture groups (not including group 0, the entire
 * match). The first capture group will match every part of the link before the
 * URL (including delimiters). The second capture group matches the URL itself.
 * The final capture group will match everything after the URL. If you intend to
 * add new LinkFormats be sure to follow the capture group conventions because
 * classes which depend on this enumeration rely on the assumption that the
 * pattern is formatted as previously specified. Attempting to define a format
 * that does not define 3 capture groups will result in an
 * IllegalArgumentException at during class load.
 *
 * @see Pattern
 */
public enum LinkFormat implements LinkPattern {

    /**
     * HTML image tag format. This format defines a regular expression pattern
     * for matching HTML image tags.
     * <p/>
     * <pre>
     *     {@code
     *     Pattern:     <img[^>]+src\s*=\s*[\'\"]([^\"]*)[^>]*>
     *     Example:     <img src="/path/to/foo.png"/>
     *
     *     Group:       ([^\"]*)
     *     Example:     /path/to/foo.png
     *     }
     * </pre>
     */
    HTML_IMAGE("<img[^>]+src\\s*=\\s*[\\'\\\"]([^\\\"]*)[^>]*>"),

    /**
     * CSS URL link. This format defines a regular expression pattern for
     * matching CSS background URLs.
     * <p/>
     * <pre>
     *     {@code
     *     Pattern:     url[\s]*\(\s*[\'\"]?([^\)\'\"]*)[\'\"]?\s*\)
     *     Example:     url("/path/to/foo.png")
     *
     *     Group:       ([^\)\'\"]*)
     *     Example:     /path/to/foo.png
     *     }
     * </pre>
     */
    CSS_URL("url[\\s]*\\(\\s*[\\'\\\"]?([^\\)\\'\\\"]*)[\\'\\\"]?\\s*\\)"),

    /**
     * DocBook imagedata file reference. This format defines a regular
     * expression pattern for matching DocBook imagedata items.
     * <p/>
     * <pre>
     *     {@code
     *     Pattern:     <imagedata[^>]+fileref\s*=\s*[\'\"]([^\"]*)[^>]*>
     *     Example:     <imagedata fileref="/path/to/foo.png"/>
     *
     *     Group:       ([^\"]*)
     *     Example:     /path/to/foo.png
     *     }
     * </pre>
     */
    DOCBOOK_IMAGEDATA("<imagedata[^>]+fileref\\s*=\\s*[\\'\\\"]([^\\\"]*)[^>]*>"),

    /**
     * Markdown inline image.
     *
     * <pre>
     *     {@code
     *     Pattern:     !\[.*?\]\([ \t]*(\S+).*?\)
     *     Example:     ![Alt text](/path/to/img.jpg "Optional title")
     *
     *     Group:       (\S+)
     *     Example:     /path/to/foo.jpg
     *     }
     * </pre>
     *
     * @see <a href="http://daringfireball.net/projects/markdown/syntax#img">Markdown Syntax</a>
     */
    MARKDOWN_INLINE_IMG("!\\[.*?\\]\\([ \\t]*(\\S+).*?\\)");

    private Pattern pattern;

    // Note sure if this is the best way to go about enforcing the capture group
    // requirements.
    LinkFormat(String regex) {
        Pattern p = Pattern.compile(regex);
        if (p.matcher("").groupCount() != 1) {
            throw new IllegalArgumentException(LinkFormat.class.getSimpleName()
                    + " instances require regular expression patterns to define"
                    + " exactly 1 capture group. Please refer to the javadoc"
                    + " for details.");
        }
        this.pattern = p;
    }

    /**
     * Get the pre-compiled regex pattern for the particular LinkFormat.
     *
     * @return Pre-compiled regex pattern of the LinkFormat.
     */
    @Override
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Get the string matching the enum constant value.
     *
     * @return Name of this link format.
     */
    @Override
    public String getPatternName() {
        return this.name();
    }
}
