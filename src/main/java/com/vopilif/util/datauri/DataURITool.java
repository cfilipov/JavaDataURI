/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.vopilif.util.datauri;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class DataURITool {

    @Option(name = "-f",
            aliases = "-file",
            required = false,
            metaVar = "<file>",
            usage = "Source file to scan for URIs to embed.")
    public File srcFile;

    @Option(name = "-o",
            aliases = "-output",
            required = false,
            metaVar = "<file>",
            usage = "Destination file containing embedded URIs.")
    public File destinationFile;

    @Option(name = "-r",
            aliases = "-root",
            required = false,
            metaVar = "<path>",
            usage = "Sets the root directory for file links.")
    public File rootDir = new File(".");

    // This *must* be an array, *not* a list or UnsupportedOperationException.
    // will be thrown. See LinkFormatOptionHandler.java for details.
    @Option(name = "-t",
            aliases = "-linktypes",
            required = false,
            handler = LinkFormatOptionHandler.class,
            usage = "Space-delimited list of link types. When scanning the "
                    + "file for links only those listed will be considered "
                    + "for embedding data URIs. By default all known link "
                    + "types are enabled.")
    public LinkFormat[] formats = LinkFormat.class.getEnumConstants();

    @Option(name = "-c",
            aliases = "-custom",
            required = false,
            metaVar = "<RE(G)EX>",
            usage = "A custom pattern to use for matching media links. The "
                    + "regular expression pattern must include exactly one "
                    + "capture group. The capture group is what will be used "
                    + "to extract the URL.")
    public String customLinkPattern;

    @Option(name = "-C",
            aliases = "-customonly",
            required = false,
            usage = "When set, only custom patterns defined in the -custom "
                    + "argument will be used to match links. If this flag is "
                    + "set and no -custom argument is provided then no links "
                    + "will be matched.")
    public boolean customLinkPatternOnly;

    @Option(name = "-h",
            aliases = "-help",
            required = false,
            usage = "Print this message.")
    public boolean help;

    public static void main(String[] args) throws IOException {
        final long startTime = System.nanoTime();

        /*
         * HANDLE OPTS
         */

        final DataURITool uriTool = new DataURITool();
        final CmdLineParser parser = new CmdLineParser(uriTool);

        if (uriTool.help) {
            parser.setUsageWidth(80);
            parser.printUsage(System.out);
            System.exit(1);
        }

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            parser.setUsageWidth(80);
            parser.printUsage(System.err);
            System.exit(1);
        }

        /*
         * PREPARE PATTERNS
         */

        final List<LinkPattern> linkPatterns = new ArrayList<LinkPattern>();

        if (uriTool.customLinkPattern != null) {
            final LinkPattern lp = new LinkPattern() {
                @Override
                public Pattern getPattern() {
                    return Pattern.compile(uriTool.customLinkPattern);
                }

                @Override
                public String getPatternName() {
                    return "CUSTOM " + uriTool.customLinkPattern;
                }
            };

            System.out.println("Custom link pattern defined: "
                    + uriTool.customLinkPattern);

            linkPatterns.add(lp);
        }

        if (!uriTool.customLinkPatternOnly) {
            linkPatterns.addAll(Arrays.asList(uriTool.formats));
        }

        final StringBuilder patternsSelected = new StringBuilder();

        for (LinkPattern p : linkPatterns) {
            patternsSelected.append(p.getPatternName()).append(" ");
        }

        System.out.println(
                "Data URI embedder will scan for the following link formats: "
                        + patternsSelected.toString());
        System.out.println(
                "Scanning file for URLs to embed: "
                        + uriTool.srcFile.getCanonicalPath());

        /*
         * EMBED DATA URIS
         */

        final StringBuilder sb = new StringBuilder();
        final BufferedReader reader = new BufferedReader(new FileReader(uriTool.srcFile));
        final DataURIEmbedder embedder = new DataURIEmbedder(reader, sb, uriTool.rootDir, linkPatterns);

        try {
            embedder.process();
        } finally {
            reader.close();
        }

        /*
         * SAVE RESULTS
         */

        if (uriTool.destinationFile == null && uriTool.srcFile != null) {
            final String path = uriTool.srcFile.getCanonicalPath();
            final int pathSepIdx = path.lastIndexOf(File.separator);
            final String dir = path.substring(0, pathSepIdx+1);
            final String fileName = path.substring(pathSepIdx+1);
            uriTool.destinationFile = new File(dir + "[embedded] " + fileName);
        }

        System.out.println("Writing output to: "
                + uriTool.destinationFile.getCanonicalPath());

        final Writer writer = uriTool.srcFile == null ?
                new OutputStreamWriter(System.out) :
                new BufferedWriter(new FileWriter(uriTool.destinationFile));

        try {
            writer.append(sb.toString());
        } finally {
            writer.close();
        }

        /*
         * DONE (timed)
         */

        final long endTime = System.nanoTime();
        final long duration = endTime - startTime;
        final double seconds = (double) duration / 1000000000.0;

        System.out.println((new StringBuilder())
                .append("[DONE] Embedded ")
                .append(embedder.getEmbedCount())
                .append(" data URIs in ")
                .append(seconds)
                .append(" seconds.")
                .toString());
    }
}
