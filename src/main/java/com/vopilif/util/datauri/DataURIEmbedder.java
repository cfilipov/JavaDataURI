/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.vopilif.util.datauri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Utility for embedding data URIs (RFC 2397) info text files.
 * <p/>
 * Inspired by Nicholas C. Zakas' CSSEmbed utility/library. CSSEmbed is a
 * utility used to replace background images in CSS with data URIs. This is
 * useful for performance but also for easily transferring HTML documents as a
 * single file. CSSEmbed focused on embedding data URIs in CSS files. This
 * utility tries to provide the flexibility of being able to embed data URIs in
 * any file format or even files with mixed formats. The embedder will accept a
 * list of {@link LinkPattern} objects which define the types of
 * links to search for and encode.
 * <p/>
 * Several parts of the code could be done more efficiently (without regex, for
 * example), however, the code was written with the goal of being easy to extend
 * to new link formats and easy to read.
 *
 * @see DataURI
 * @see LinkFormat
 * @see LinkPattern
 * @see <a href="http://www.ietf.org/rfc/rfc2397.txt">RFC 2397 (IETF)</a>
 * @see <a href="https://github.com/nzakas/cssembed/">nzakas / cssembed (github)</a>
 */
public class DataURIEmbedder {

    private final BufferedReader reader;
    private final LinkPattern[] linkPatterns;
    private final Appendable writer;
    private final Matcher matcher;
    private final File rootDir;

    private int embedCount;

    /**
     * Create a DataURIEmbedder for a given {@link BufferedReader} input source.
     * The process of embedding will not commence until
     * {@link com.vopilif.util.datauri.DataURIEmbedder#process()}
     * is called.
     *
     * @param reader       Input source. It is the caller's responsibility to
     *                     setup the reader and open/close the stream.
     * @param writer       Any implementation of {@link Appendable} which will
     *                     be used to store the output file contents.
     * @param rootDir      A File path to search for media files for local
     *                     file references.
     * @param linkPatterns Aa Array of {@link LinkPattern} items representing
     *                     the type of links to turn into data URIs.
     */
    public DataURIEmbedder(BufferedReader reader, Appendable writer, File rootDir, LinkPattern... linkPatterns) {
        assert linkPatterns.length > 0;
        this.embedCount = 0;
        this.reader = reader;
        this.writer = writer;
        this.linkPatterns = linkPatterns;
        this.matcher = linkPatterns[0].getPattern().matcher("");
        this.rootDir = rootDir;
    }

    /**
     * Convenience constructor which takes a List as input.
     *
     * @param reader       Input source. It is the caller's responsibility to
     *                     setup the reader and open/close the stream.
     * @param writer       Any implementation of {@link Appendable} which will
     *                     be used to store the output file contents.
     * @param rootDir      A File path to search for media files for local file
     *                     references.
     * @param linkPatterns A list of {@link LinkPattern} items representing the
     *                     type of links to turn into data URIs.
     */
    public DataURIEmbedder(BufferedReader reader, Appendable writer, File rootDir, List<LinkPattern> linkPatterns) {
        this(reader, writer, rootDir, linkPatterns.toArray(new LinkPattern[linkPatterns.size()]));
    }

    /**
     * Searches the stream for matching link formats and attempts to replace
     * them with data URIs.
     *
     * @throws IOException
     */
    public void process() throws IOException {
        for (String line = this.reader.readLine(); line != null; line = this.reader.readLine()) {
            for (LinkPattern linkPattern : this.linkPatterns) {
                this.matcher.reset(line).usePattern(linkPattern.getPattern());
                while (this.matcher.find()) {
                    final String url = this.matcher.group(1);
                    final String dataURI = this.getDataURI(url);
                    if (dataURI != null) {
                        line = line.replace(url, dataURI);
                        this.embedCount++;
                    }
                }
            }
            this.writer.append(line).append("\n");
        }
    }

    /**
     * Counts the number of data URIs that have been embedded during processing.
     *
     * @return Number of dataURIs embedded.
     */
    public int getEmbedCount() {
        return embedCount;
    }

    // TODO: Memoize data URIs
    private String getDataURI(String url) throws IOException {
        String dataURI = null;
        if (url.startsWith("data:")) {
            System.out.println("Existing data URI:\t" + url.substring(0, 64));
            dataURI = url;

        } else if (url.startsWith("http:")) {
            System.out.println("Fetching URL:\t" + url);
            dataURI = DataURI.encode(new URL(url));
            System.out.println("\t" + dataURI.substring(0, 64) + "...");

        } else {
            final File file = new File(this.rootDir, url);

            try {
                System.out.println("Locating file:\t" + file.getCanonicalPath());
                dataURI = DataURI.encode(file);
                System.out.println("\t" + dataURI.substring(0, 64) + "...");

            } catch (FileNotFoundException e) {
                System.err.println("[ERROR] File not found: " + url);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dataURI;
    }
}