/*
 * Copyright (c) 2012 Cristian Filipov. All rights reserved.
 * http://www.vopilif.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Neither the name of the owner nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.vopilif.util.datauri;


import java.util.regex.Pattern;

/**
 * Interface used by {@link LinkFormat} and {@link DataURITool} to define regex
 * patterns for matching hyperlinks and their URLs.
 * Instances of this interface must return a {@link Pattern} containing one
 * capture group (not including group 0, the entire match). It is important
 * this group capture the URL in the link.
 *
 * @see Pattern
 */
public interface LinkPattern {

    /**
     * Compiled pattern used to match links in text documents.
     *
     * @return A {@link Pattern} containing a regular expression which defines
     *         one capture group (not including group 0, the entire match). The
     *         capture group will match the URL inside the link.
     */
    Pattern getPattern();

    /**
     * @return A name given to the specific pattern.
     */
    String getPatternName();
}
